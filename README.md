# 2017_Alignments

## Weekly Updates

# Till November 21st 2024
Lorentz Correction of MP:![Lorentz Correction of MP](Alon/LorentzExp.png)


Applying the alignments to the magnetic field on runs and corrections for the MA:![MA](Alon/Nov2024_MAExp.png)

and GM10 and GM11:![MA](Alon/Nov2024_GMExp.png)

Full PDFs can be found in alon/ sub dir. 

# To September 20th 2024

We had major issues with the MAs 


MAs before fixes:![MA_beforeFix](UnbiasedPlots/September20th/MA_old.png)


This lead to us using the unbiased procedure. This fixed the MAs



MA (DuVsU) fixed many of the issues affecting the MAs from past alignments:![MA](UnbiasedPlots/September20th/MA_fixed.png)


However, other isssues still arose like W45s which were fixed using the unbiased procedure:


DW (DuVsU) some of the fixed W45 Plots:![DW](UnbiasedPlots/September20th/DW_fixed.png)

Some issues still exist even after unbiased. Have to speak with Renat but we might have to move on to the magnetic field on runs and 
run the biased procedure with those to fix some of these issues.


DW (DuVsV) issues still exist :![DW](UnbiasedPlots/September20th/DW_Issues.png)




# May 27th- 31st:
Fixed most of the major issues with the aloff runs. Examples below:

GM:![Example of GM](aloff/fixed/GM.png)
MP:![Example of MP](aloff/fixed/MP.png)
DW:![Example of DW](aloff/fixed/DW.png)
MA:![Example of MA](aloff/fixed/MA.png)
MB:![Example of MB](aloff/fixed/MB.png)

# Up until May 24th: 
Alignment procedure was started from scratch:

Ran the Postion (T) and Angle (A) alignments on  GM, FI,MP, DC, inner ST, W45, MA, MB, Si, and GP.
![Example of GM](FirstLookPlots/outCompareGM.png)

![All PDFs for each detector can be found here](FirstLookPlots/)